package com.testcigitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCiGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCiGitlabApplication.class, args);
	}

}
